import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
    selector: "dashboard-page",
    templateUrl: './dashboard.component.html',
    styleUrls: [ './dashboard.scss' ]
})

export class DashboardComponent implements OnInit,OnDestroy {
    constructor() {}
    ngOnInit(){}
    ngOnDestroy(){}
}